Source: steptalk
Section: gnustep
Priority: optional
Maintainer: Debian GNUstep maintainers <pkg-gnustep-maintainers@lists.alioth.debian.org>
Uploaders:
 Yavor Doganov <yavor@gnu.org>,
Build-Depends:
 debhelper-compat (= 13),
 libgnustep-dl2-dev (>= 0.12.0+git20171224-4),
 libgnustep-gui-dev (>= 0.31.1-7),
 libreadline-dev,
 libsqlclient-dev (>= 1.9.0-4),
Build-Depends-Indep:
 gnustep-base-doc <!nodoc>,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: http://wwwmain.gnustep.org/experience/StepTalk.html
Vcs-Git: https://salsa.debian.org/gnustep-team/steptalk.git
Vcs-Browser: https://salsa.debian.org/gnustep-team/steptalk

Package: steptalk
Architecture: any
Depends:
 libsteptalk0d (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 steptalk-doc,
Description: GNUstep Scripting Framework (tools)
 StepTalk is a scripting framework for creating scriptable servers or
 applications.  StepTalk, when combined with the dynamism of the Objective-C
 language, goes way beyond mere scripting.
 .
 This package contains the StepTalk tools.

Package: steptalk-doc
Architecture: all
Build-Profiles: <!nodoc>
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends},
Suggests:
 gnustep-base-doc,
Breaks:
 steptalk (<< 0.10.0+git20200629-1~),
Replaces:
 steptalk (<< 0.10.0+git20200629-1~),
Description: GNUstep Scripting Framework (documentation)
 StepTalk is a scripting framework for creating scriptable servers or
 applications.  StepTalk, when combined with the dynamism of the Objective-C
 language, goes way beyond mere scripting.
 .
 This package contains the documentation and some examples.

Package: libsteptalk0d
Section: libs
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 steptalk-gui-module,
Suggests:
 steptalk-gdl2-module,
 steptalk-sqlclient-module,
Replaces:
 libsteptalk0,
Breaks:
 libsteptalk0,
Description: GNUstep Scripting Framework (library files)
 StepTalk is a scripting framework for creating scriptable servers or
 applications.  StepTalk, when combined with the dynamism of the Objective-C
 language, goes way beyond mere scripting.
 .
 This package contains the shared library.

Package: libsteptalk-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libgnustep-base-dev,
 libsteptalk0d (= ${binary:Version}),
 ${misc:Depends},
Description: GNUstep Scripting Framework (development files)
 StepTalk is a scripting framework for creating scriptable servers or
 applications.  StepTalk, when combined with the dynamism of the Objective-C
 language, goes way beyond mere scripting.
 .
 This package contains the development files and headers.

Package: steptalk-gui-module
Architecture: any
Multi-Arch: same
Depends:
 libsteptalk0d (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 steptalk,
Replaces:
 libsteptalk0,
Breaks:
 libsteptalk0,
Description: GNUstep Scripting Framework (GUI module)
 StepTalk is a scripting framework for creating scriptable servers or
 applications.  StepTalk, when combined with the dynamism of the Objective-C
 language, goes way beyond mere scripting.
 .
 This package contains the ApplicationScripting bundle, the
 ApplicationFinder and the GNUstep GUI (a.k.a. AppKit) support module.

Package: steptalk-gdl2-module
Architecture: any
Multi-Arch: same
Depends:
 libsteptalk0d (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 steptalk,
Replaces:
 libsteptalk0,
Breaks:
 libsteptalk0,
Description: GNUstep Scripting Framework (GDL2 module)
 StepTalk is a scripting framework for creating scriptable servers or
 applications.  StepTalk, when combined with the dynamism of the Objective-C
 language, goes way beyond mere scripting.
 .
 This package contains the GNUstep Database Library (GDL2) support module.

Package: steptalk-sqlclient-module
Architecture: any
Multi-Arch: same
Depends:
 libsteptalk0d (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 steptalk,
Description: GNUstep Scripting Framework (SQLClient module)
 StepTalk is a scripting framework for creating scriptable servers or
 applications.  StepTalk, when combined with the dynamism of the Objective-C
 language, goes way beyond mere scripting.
 .
 This package contains the SQLClient support module.
